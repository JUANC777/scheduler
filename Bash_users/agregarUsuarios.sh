#!/bin/bash
ROOT_UID=0
SUCCESS=0

# Run as root, of course. (this might not be necessary, because we have to run the script somehow with root anyway)
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Se debe estar como root para ejecutar este script"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe indicar el archivo con el listado de usuarios a agregar..."
   exit 1
fi

# Del archivo con el listado de usuarios a agregar:
# Este es el formato:
# ejemplo
#    |   
#    f1  
#$f1 = username

agregarUsuario(){
	#echo "----> Agregar Usuario <----"
	eval user="$1"
	#echo "Username 		  = ${user}"
	#echo "-------------------------"

	useradd -r "${user}"
	if [ $? -eq $SUCCESS ];
	then
		echo "Usuario [${user}] agregado correctamente..."
	else
		echo "Usuario [${user}] No se pudo agregar..."
	fi
}

while IFS=: read -r f1
do
	agregarUsuario "\${f1}"	
done < ${file}

exit 0
