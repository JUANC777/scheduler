
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>


int conexion (char *server)
{
	struct sockaddr_un dir;
	int crip;

	strcpy (dir.sun_path, server);
	dir.sun_family = AF_UNIX;

	crip = socket (AF_UNIX, SOCK_STREAM, 0);
	if (crip == -1)
		return -1;


	if (connect (
			crip,
			(struct sockaddr *)&dir,
			strlen (dir.sun_path) + sizeof (dir.sun_family)) == -1)
	{
		return -1;
	}
	else
	{
		return crip;
	}
}


int conexion_red (
	char *host_red,
	char *server)
{
	struct sockaddr_in dir;
	struct servent *port;
	struct hostent *host;
	int crip;

	port = getservbyname (server, "tcp");
	if (port == NULL)
		return -1;

	host = gethostbyname (host_red);
	if (host == NULL)
		return -1;

	dir.sin_family = AF_INET;
	dir.sin_addr.s_addr = ((struct in_addr *)(host->h_addr))->s_addr;
	dir.sin_port = port->s_port;

	crip = socket (AF_INET, SOCK_STREAM, 0);
	if (crip == -1)
		return -1;

	if (connect (
			crip,
			(struct sockaddr *)&dir,
			sizeof (dir)) == -1)
	{
		return -1;
	}

	return crip;
}
