
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>


int soc_conexion (char *server)
{
	struct sockaddr_un dir;
	int des;


	des = socket (AF_UNIX, SOCK_STREAM, 0);
	if (des == -1)
	 	return -1;


	strcpy (dir.sun_path, server);
	dir.sun_family = AF_UNIX;

	if (bind (
			des,
			(struct sockaddr *)&dir,
			strlen (dir.sun_path) + sizeof (dir.sun_family)) == -1)
	{

		close (des);
		return -1;
	}


	if (listen (des, 1) == -1)
	{
		close (des);
		return -1;
	}


	return des;
}


int con_cliente (int des)
{
	socklen_t Longitud_Cliente;
	struct sockaddr Cliente;
	int Hijo;


	Longitud_Cliente = sizeof (Cliente);
	Hijo = accept (des, &Cliente, &Longitud_Cliente);
	if (Hijo == -1)
		return -1;


	return Hijo;
}


int soc_conexion_red (char *server)
{
	struct sockaddr_in dir;
	struct sockaddr Cliente;
	socklen_t Longitud_Cliente;
	struct servent *port;
	int des;


	des = socket (AF_INET, SOCK_STREAM, 0);
	if (des == -1)
	 	return -1;


	port = getservbyname (server, "tcp");
	if (port == NULL)
		return -1;


	dir.sin_family = AF_INET;
	dir.sin_port = port->s_port;
	dir.sin_addr.s_addr =INADDR_ANY;
	if (bind (
			des,
			(struct sockaddr *)&dir,
			sizeof (dir)) == -1)
	{
		close (des);
		return -1;
	}


	if (listen (des, 1) == -1)
	{
		close (des);
		return -1;
	}


	return des;
}
