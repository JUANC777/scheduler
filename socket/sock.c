
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>




int ingresar (int a, char *dato, int ancho)
{
	int salida = 0;
	int i = 0;


	if ((a == -1) || (dato == NULL) || (ancho < 1))
		return -1;


	while (salida < ancho)
	{
		i = read (a, dato + salida, ancho - salida);
		if (i > 0)
		{

			salida = salida + i;
		}
		else
		{

			if (i == 0)
				return salida;
			if (i == -1)
			{

				switch (errno)
				{
					case EINTR:
					case EAGAIN:
						usleep (100);
						break;
					default:
						return -1;
				}
			}
		}
	}


	return salida;
}


int escribir (int b, char *dato, int ancho)
{
	int salidaB = 0;
	int i = 0;


	if ((b == -1) || (dato == NULL) || (ancho < 1))
		return -1;


	while (salidaB < ancho)
	{
		i = write (b, dato + salidaB, ancho - salidaB);
		if (i > 0)
		{

			salidaB = salidaB + i;
		}
		else
		{

			if (i == 0)
				return salidaB;
			else
				return -1;
		}
	}


	return salidaB;
}
